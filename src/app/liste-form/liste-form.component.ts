import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-form',
  templateUrl: './liste-form.component.html',
  styleUrls: ['./liste-form.component.scss']
})
export class ListeFormComponent implements OnInit {
  form: FormGroup;
  message = "";
  codetab= [];
  codes = this.http.get<any[]>('http://localhost:4201');
  codesval = this.codes.subscribe(lecode => {this.codetab = lecode})

  constructor(private http: HttpClient, private router:Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      code: new FormControl()
    })
    console.log(this.codesval[0])
  }

  getCode(){
    let theCode;
    for(let code of this.codetab){
      theCode = code.code;
    }

    if (theCode == this.form.value.code) {
      this.message = 'Code bon';
      this.router.navigate(['admin'])
    }
    else {
      this.message = 'code incorrect'
    }
  }

}
