import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Client } from '../client';
import { ClientService } from '../client.service';

@Component({
  selector: 'app-datas',
  templateUrl: './datas.component.html',
  styleUrls: ['./datas.component.scss']
})
export class DatasComponent implements OnInit {
  clients : any

  constructor(private http: HttpClient, private clientService: ClientService) { }
  ngOnInit(): void {
    this.clientService.fetchClients().subscribe(
      response => {
        console.log(response);
        this.clients = response
      }
    )
  }

}
