import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { ClientService } from '../client.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  message = "";
  lesClients: any = []
  allClients: Client[];
 
  

  constructor(private http: HttpClient, private clientService: ClientService) { }
  printTOUT: string

  ngOnInit(): void {
      this.allClients = this.clientService.getALLClients()
      console.log(this.allClients)
    
  }
  
  deleteAllClients(){
    this.clientService.deleteClient()
    this.allClients = this.clientService.getALLClients()
  }

  addClient(){
    for(let i = 0; i < this.allClients.length; i++){
    this.clientService.addClientsInDB(this.allClients[i])
    }
  }
}
