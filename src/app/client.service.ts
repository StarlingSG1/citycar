import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Client } from './client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private clients: Client[];
  private fetchedClients: any = []
  constructor(private http: HttpClient) { this.clients = []; }

  
  getALLClients() {
    this.clients = JSON.parse(localStorage.getItem('Clients') || '[]');
    return this.clients;
  }

  addClient(client: Client) {
    this.clients = this.getALLClients()
    this.clients.push(client);
    let tabItems = JSON.stringify(this.clients);
    localStorage.setItem('Clients', tabItems);
  }

  deleteClient(){
    localStorage.removeItem('Clients')
  }

  public fetchClients(){
    return this.http.get(environment.url + environment.fetch)
  }

  public addClientsInDB(client:Client){
    console.log(client)
    return this.http.post(environment.url + environment.addClient,client ).subscribe()
    }
}
