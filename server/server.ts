
const express = require('express')
const mongoose = require('mongoose')
const app = express();
const Data = require('./serviceSchema')
const bodyParser = require('body-parser');

mongoose.connect('mongoDB://localhost/cityCarDB')

mongoose.connection.once("open", () => {
    console.log("Connected to DB!")
}).on("error", (error) => {
    console.log("Failed to connect" + error)
})



app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
    if('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        console.log(`${req.ip} ${req.method} ${req.url}`);
        next();
    }
});



app.post('/create',  (req, res) => {
    console.log(req.body.nom)
    const service = new Data({
        nom: req.body.nom,
        service: req.body.service,
    })
    service.save((err : Error) => {
        if(err){
            res.status(400).json({message: "Erreur : Il y a une erreur "})
        }else{
            res.status(200).json({message: "Saved data!"})
        }
    });
    
});







app.get('/', (req, res) => {
    res.send([{code: 1234}]);
});

app.get('/fetch', (req, res) => {
    Data.find({}).then((DBitems) => {
        res.send(DBitems)
        console.log(DBitems)
    })
    console.log("METHODE")
})

app.post("/delete", (req, res)=> {
    Data.findOneAndRemove({
        _id: req.get("id")
    }, (err) => { 
        console.log("Failed" + err)
    })

    res.send("Deleted!")
})



app.listen(4201, '127.0.0.1',{ useNewUrlParser: true, useUnifiedTopology: true }, function(){
    console.log('le serveur écoute bien le port 4201');
});




